import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import { apiURL } from '../../config';
import { CardContent, Typography, Button, CardActions } from '@material-ui/core';

const useStyles = makeStyles({
    root: {
        width: "200px",
        height: "auto",
        margin: "10px 0",
        padding: "10px",
        border: "1px solid",
        borderColor: "rgb(187, 195, 204)",
        textAlign: "center",
    },
    user: {
        textDecoration: "underline",
        color: "rgb(0, 123, 255)",
        '&:hover': {
            color: 'rgb(187, 195, 204)'
        }
    }
});

const Photo = (props) => {
    const classes = useStyles();

    let currentUserId = "";
    if(props.currentUser) {
        currentUserId = props.currentUser._id;
    }

    let user = "";
    let userId = "";
    if(props.user) {
        user = props.user.displayName;
        userId = props.user._id;
    }

    let image = "";
    if(props.image) {
        image = apiURL + "/uploads/" + props.image;
    }

    return (
        <>
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt={props.name}
                    height="200"
                    image={image}
                    onClick={props.imageHandler}
                />
                <CardContent className={classes.content}>
                    <Typography gutterBottom variant="h5" component="h2">
                        {props.title}
                    </Typography>
                    {props.gallery ? null : (
                        <Typography variant="body2" color="textSecondary" component="p" className={classes.user} onClick={props.authorHandler}>
                            By {user}
                        </Typography>
                    )}
                </CardContent>
            </CardActionArea>
            {(props.gallery && userId === currentUserId) ? (
                <CardActions>
                    <Button variant="contained" color="secondary" onClick={props.removePhotoHandler}>
                        Delete
                    </Button>
                </CardActions>
                ) : null}
        </Card>
        </>
    );
}

export default Photo;