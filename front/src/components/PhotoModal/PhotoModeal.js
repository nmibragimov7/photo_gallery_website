import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

const useStyles = makeStyles({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const PhotoModal = (props) => {
    const classes = useStyles();

    return (
        <>
            <Modal
                open={props.modal}
                onClose={props.handleClose}
                className={classes.modal}
            >
                {props.children}
            </Modal>
        </>
    );
}

export default PhotoModal;