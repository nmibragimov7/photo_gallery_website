import {
    RESET_ERROR,
    RESET_MESSAGE,
    FETCH_PHOTOS_SUCCESS,
    ADD_PHOTO_SUCCESS,
    ADD_PHOTO_FAILURE,
    DELETE_PHOTO_SUCCESS,
    DELETE_PHOTO_FAILURE
} from "../actions/photosActions";

const initialState = {
    photos: [],
    photo: {},
    errors: null,
    message: null,
};

const photosReducer = (state = initialState, action) => {
    switch(action.type){
        case RESET_ERROR:
            return {...state, errors: null};
        case RESET_MESSAGE:
            return {...state, message: null};
        case FETCH_PHOTOS_SUCCESS:
            return {...state, photos: action.photos}
        case ADD_PHOTO_SUCCESS:
            return {...state, errors: null, message: action.message}
        case ADD_PHOTO_FAILURE:
            return {...state, errors: action.errors}
        case DELETE_PHOTO_SUCCESS:
            return {...state, errors: null, message: action.message}
        case DELETE_PHOTO_FAILURE:
            return {...state, errors: action.errors}
        default:
            return state;
    }
};

export default photosReducer;