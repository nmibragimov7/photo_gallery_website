import axios from "../../axiosApi";
import { loadingOffHandler } from "./loadingActions";
import { push } from "connected-react-router";

export const RESET_ERROR = 'RESET_ERROR';
export const RESET_MESSAGE = 'RESET_MESSAGE';
export const FETCH_PHOTOS_SUCCESS = 'FETCH_PHOTOS_SUCCESS';
export const ADD_PHOTO_SUCCESS = 'ADD_PHOTO_SUCCESS';
export const ADD_PHOTO_FAILURE = 'ADD_PHOTO_FAILURE';
export const DELETE_PHOTO_SUCCESS = 'DELETE_PHOTO_SUCCESS';
export const DELETE_PHOTO_FAILURE = 'DELETE_PHOTO_FAILURE';

export const resetError = () => {
    return {type: RESET_ERROR}
};
export const resetMessage = () => {
    return {type: RESET_MESSAGE}
};
export const fetchPhotosSuccess = (photos) => {
    return {type: FETCH_PHOTOS_SUCCESS, photos}
};
export const addPhotoSuccess = (message) => {
    return {type: ADD_PHOTO_SUCCESS, message}
};
export const addPhotoFailure = (errors) => {
    return {type: ADD_PHOTO_FAILURE, errors}
};
export const deletePhotoSuccess = (message) => {
    return {type: DELETE_PHOTO_SUCCESS, message}
};
export const deletePhotoFailure = (errors) => {
    return {type: DELETE_PHOTO_FAILURE, errors}
};

export const fetchPhotos = () => {
    return async dispatch => {
        try {
            const response = await axios.get('/photos');
            dispatch(fetchPhotosSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const fetchPhotosByUser = (id) => {
    return async dispatch => {
        try {
            const response = await axios.get(`/photos/${id}`);
            dispatch(fetchPhotosSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const addPhoto = (photoData) => {
    return async dispatch => {
        try {
            const response = await axios.post(`/photos`, photoData);
            dispatch(addPhotoSuccess(response.data));
            dispatch(push('/'));
        } catch(e) {
            dispatch(addPhotoFailure(e.response.data));
            console.error(e);
        }
    }
};

export const deletePhoto = (id) => {
    return async (dispatch, getState) => {
        try {
            const response = await axios.delete(`/photos/${id}`);
            dispatch(deletePhotoSuccess(response.data));
            if(getState().users) {
                const id = getState().users.user._id;
                dispatch(fetchPhotosByUser(id));
            }
        } catch(e) {
            dispatch(deletePhotoFailure(e.response.data));
            console.error(e);
        }
    }
};