import React from 'react';
import Container from "@material-ui/core/Container";
import { Route, Switch, Redirect } from "react-router-dom"
import AppToolbar from "./components/AppToolbar/AppToolbar";
import { useSelector } from "react-redux";
import Register from "./containers/User/Register";
import Login from './containers/User/Login';
import Photos from './containers/Photos/Photos';
import PersonGallery from './containers/PersonGallery/PersonGallery';
import NewPhoto from './containers/NewPhoto/NewPhoto';

const App = () => {
  const user = useSelector(store => store.users.user);

  return(
    <>
      <header>
        <AppToolbar user={user}/>
      </header>
      <main>
        <Container>
          <Switch>
            <Route path="/" exact component={Photos}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <ProtectRoute path="/my_gallery" isAllowed={user} redirectTo={'/'} exact component={PersonGallery}/>
            <ProtectRoute path="/new_photo" isAllowed={user} redirectTo={'/'} exact component={NewPhoto}/>
            <Route path="/:id" exact component={PersonGallery}/>
            <Route path='/' render={()=>(<div><h1>404 Not found</h1></div>)}/>
          </Switch>
        </Container>
      </main>
    </>
  );
};

const ProtectRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
      <Route {...props} /> :
      <Redirect to={redirectTo} />
};

export default App;
