import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useDispatch, useSelector } from "react-redux";
import TextInput from "../../components/UI/Form/TextInput";
import FileInput from '../../components/UI/Form/FileInput';
import { resetError, addPhoto } from '../../store/actions/photosActions';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const NewPhoto = () => {
    const classes = useStyles();
    const [state, setState] = useState({
        title: "",
        image: "",
    });

    const dispatch = useDispatch();
    const errors = useSelector(state => state.photos.errors);

    useEffect(()=> {
        dispatch(resetError());
    }, [dispatch]);

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState =>({
            ...prevState,
            [name]: file
            })
        )
    };

    const inputHandler = event => {
        const {name, value} = event.target;
        setState(prevState => {
            return {...prevState, [name]: value}
        });
    };

    const formSubmitHandler = async event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key])
        });
        await dispatch(addPhoto(formData));
    };

    const getFieldError = fieldName => {
        try {
            return errors.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Add new photo
                </Typography>
                <form className={classes.form} noValidate onSubmit={formSubmitHandler}>
                    <Grid container spacing={2}>
                        <TextInput
                            label="title"
                            onChange={inputHandler}
                            name="title"
                            error={getFieldError('title')}
                            required={true}
                        />
                        <Grid item>
                            <FileInput
                                name="image"
                                label="image"
                                onChange={fileChangeHandler}
                                error={getFieldError('image')}
                                required={true}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Create photo
                    </Button>
                </form>
            </div>
        </Container>
    );
}

export default NewPhoto;