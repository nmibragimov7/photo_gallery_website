import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Photo from '../../components/Photo/Photo';
import { fetchPhotosByUser, resetError, deletePhoto, resetMessage } from '../../store/actions/photosActions';
import { loadingHandler } from '../../store/actions/loadingActions';
import { Grid, Button } from '@material-ui/core';
import Spinner from '../../components/Spinner/Spinner';
import PhotoModal from '../../components/PhotoModal/PhotoModeal';
import { apiURL } from '../../config';
import { fetchUser } from '../../store/actions/usersAction';
import { NavLink } from "react-router-dom";
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    },
    modal: {
        background: "#fff",
        width: "50%",
        height: "75%",
        display: "flex",
        flexDirection: "column",        
    },
    image: {
        width: "100%",
        height: "auto"
    }
}));

const PersonGallery = (props) => {
    const classes = useStyles();
    const [imageBlock, setImageBlock] = useState((<></>));
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const userGallery = useSelector(state => state.users.userGallery);
    const photos = useSelector(state => state.photos.photos);
    const loading = useSelector(state => state.loading.loading);
    const errors = useSelector(state => state.photos.errors);
    const message = useSelector(state => state.photos.message);

    const [modal, setModal] = useState(false);
    const handleClose = () => {
        setModal(false);
    };
    let userName = "";
    if(!props.match.params.id) {
        userName = user.displayName;
    } else {
        if(userGallery) {
            userName = userGallery.displayName;
        }
    }
    let userId = "";
    if(user) {
        userId = user._id;
    }
    let userGalleryId = "";
    if(userGallery) {
        userGalleryId = userGallery._id;
    }

    useEffect(()=> {
        dispatch(loadingHandler());
        dispatch(resetError());
        if(!props.match.params.id) {
            dispatch(fetchPhotosByUser(user._id));
        } else {
            dispatch(fetchUser(props.match.params.id));
            dispatch(fetchPhotosByUser(props.match.params.id));
        }
    }, [dispatch, props.match.params.id, user]);

    const imageHandler = (image) => {
        const imageURL = apiURL + "/uploads/" + image;
        const imageBlock = (
            <div className={classes.modal}>
                <img src={imageURL} alt={image} className={classes.image}/>
                <Button onClick={handleClose} variant="contained" color="primary">Close</Button>
            </div>
        );
        setImageBlock(imageBlock);
        setModal(true);
    };

    const removePhotoHandler = (id) => {
        dispatch(deletePhoto(id));
    };

    const closeAlert = () => {
        dispatch(resetMessage());
    };

    let form = (
        <>
            <Grid 
            container 
            direction="row"
            justify="space-between"
            alignItems="center"
            spacing={2}>
                <Grid item>
                    <h1>{userName}'s gallery</h1>
                </Grid>
                <Grid item>
                    {(userId === userGalleryId || !props.match.params.id) ? (
                        <Button to="/new_photo" className={classes.link} color="inherit" variant="outlined" component={NavLink}>Add new photo</Button>
                    ) : null}
                </Grid>
            </Grid>
            <Grid 
            container 
            direction="row"
            alignItems="center"
            spacing={2}>
                {photos.map((photo) => (
                    <Grid item key={photo._id}>
                        <Photo
                        currentUser={user}
                        key={photo._id}
                        user={photo.user}
                        title={photo.title}
                        image={photo.image}
                        imageHandler={() => imageHandler(photo.image)}
                        removePhotoHandler={() => removePhotoHandler(photo._id)}
                        gallery={true}
                        />
                    </Grid>
                ))}
            </Grid>
        </>
    )

    if(loading) {
        form = <Spinner />;
    }

    return (
        <>
            <PhotoModal modal={modal} handleClose={handleClose}>
                {imageBlock}
            </PhotoModal>
            {errors && errors.error &&
                <Alert
                    severity="error"
                    className={classes.alert}
                >
                    {errors.error}
                </Alert>
            }
            {message && message.message &&
                <Alert 
                onClose={closeAlert}
                severity="success"
                className={classes.alert}
                >
                    {message.message}
                </Alert>
            }
            {form}
        </>
    );
};

export default PersonGallery;