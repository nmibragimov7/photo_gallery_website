import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Photo from '../../components/Photo/Photo';
import { fetchPhotos, resetError, resetMessage } from '../../store/actions/photosActions';
import { loadingHandler } from '../../store/actions/loadingActions';
import { Grid, Button } from '@material-ui/core';
import Spinner from '../../components/Spinner/Spinner';
import PhotoModal from '../../components/PhotoModal/PhotoModeal';
import { apiURL } from '../../config';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    },
    modal: {
        background: "#fff",
        width: "50%",
        height: "75%",
        display: "flex",
        flexDirection: "column",        
    },
    image: {
        width: "100%",
        height: "auto"
    }
}));

const Photos = (props) => {
    const classes = useStyles();
    const [imageBlock, setImageBlock] = useState((<></>));
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    const loading = useSelector(state => state.loading.loading);
    const message = useSelector(state => state.photos.message);

    const [modal, setModal] = useState(false);
    const handleClose = () => {
        setModal(false);
    };

    useEffect(()=> {
        dispatch(loadingHandler());
        dispatch(resetError());
        dispatch(fetchPhotos());
    }, [dispatch]);

    const imageHandler = (image) => {
        const imageURL = apiURL + "/uploads/" + image;
        const imageBlock = (
            <div className={classes.modal}>
                <img src={imageURL} alt={image} className={classes.image}/>
                <Button onClick={handleClose} variant="contained" color="primary">Close</Button>
            </div>
        );
        setImageBlock(imageBlock);
        setModal(true);
    };

    const authorHandler = (userId) => {
        props.history.push({
            pathname: `/${userId}`,
        });
    };

    const closeAlert = () => {
        dispatch(resetMessage());
    };

    let form = (
        <Grid 
        container 
        direction="row"
        alignItems="center"
        spacing={2}>
            {photos.map((photo) => (
                <Grid item key={photo._id}>
                    <Photo
                    key={photo._id}
                    user={photo.user}
                    title={photo.title}
                    image={photo.image}
                    imageHandler={() => imageHandler(photo.image)}
                    authorHandler={() => authorHandler(photo.user._id)}
                    />
                </Grid>
            ))}
        </Grid>
    )

    if(loading) {
        form = <Spinner />;
    }

    return (
        <>
            <PhotoModal modal={modal} handleClose={handleClose}>
                {imageBlock}
            </PhotoModal>
            {message && message.message &&
                <Alert 
                onClose={closeAlert}
                severity="success"
                className={classes.alert}
                >
                    {message.message}
                </Alert>
            }
            {form}
        </>
    );
};

export default Photos;