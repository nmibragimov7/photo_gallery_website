const mongoose = require('mongoose');
const config = require('./app/config');
const User = require('./app/models/User');
const Photo = require('./app/models/Photo');
const { nanoid } = require('nanoid');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('photos');
    } catch (e) {
        console.log('Collection not found. Drop collections skiped...');
    }

    const [user1, user2] = await User.create({
        username: "admin",
        displayName: "Admin",
        password: "admin",
        avatarImage: "image.png",
        token: nanoid(),
    }, {
        username: "user",
        displayName: "User",
        password: "user",
        avatarImage: "image.png",
        token: nanoid(),
    });

    await Photo.create({
        user: user1._id,
        title: "Photo1",
        image: "photo1.jpg",
    }, {
        user: user2._id,
        title: "Photo2",
        image: "photo2.jpg",
    });

    await db.close();
});