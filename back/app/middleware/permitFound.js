const Photo = require('../models/Photo');

const permitFound = async (req, res, next) => {
    let photo = null
    try {
        photo = await Photo.findOne({_id: req.params.id});
    } catch (error) {
        return res.status(403).send({error: "Photo is not found"});
    }

    req.photo = photo;
    next();
};

module.exports = permitFound;