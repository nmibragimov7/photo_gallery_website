const express = require('express');
const multer = require('multer');
const router = express.Router();
const { nanoid } = require('nanoid');
const path = require('path');
const config = require('./config');
const Photo = require('./models/Photo');
const auth = require('./middleware/auth');
const mongoose = require('mongoose');
const permitFound = require('./middleware/permitFound');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});
const createRouter = () =>{

    router.get('/', async (req, res) => {
        try {
            const photos = await Photo.find().populate("user");
            res.send(photos);
        } catch (e) {
            res.status(500).send(e);
        }
    });
    router.get('/:id', async (req, res) => {
        try {
            const photos = await Photo.find({user: req.params.id}).populate("user");
            
            res.send(photos);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.post('/', auth, upload.single('image'), async (req, res) => {
        const photo = new Photo({
            title: req.body.title
        });
        if(req.file){
            photo.image = req.file.filename;
        }
        photo.user = req.user._id;

        try{
            await photo.save();
            res.send({message: "Photo added successful"});
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.delete('/:id', auth, permitFound, async (req, res) => {
        if(req.user._id.toString() !== req.photo.user.toString()) {
            return res.status(403).send({error: "Photo cannot be deleted by this user"});
        }
        
        await Photo.findOneAndRemove({_id: req.photo._id}, function (e, docs) { 
            if (e){ 
                return res.status(403).send({error: "Photo cannot be deleted"});
            } 
        });

        try{
            res.send({message: "Cocktail is deleted successful"});
        } catch (e) {
            res.status(500).send(e);
        }
    });
    
    return router;
};

module.exports = createRouter;